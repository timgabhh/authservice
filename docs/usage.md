# Use GID-SSO in your App

## What is SSO?

Single sign-on (SSO) is a mechanism that allows users to access multiple applications or systems using a single set of login credentials. Instead of requiring users to remember and enter different usernames and passwords for each application, SSO enables them to authenticate once and gain access to various services seamlessly.

## How does SSO work?

1. User initiates login: The user attempts to access an application or service that is part of the SSO system.
2. Redirect to Identity Provider (IDP): The application recognizes that the user needs to authenticate and redirects them to the central IDP.
3. User authentication: The user enters their credentials (username and password) or uses an alternative authentication method (such as biometrics) at the IDP.
4. Issuance of token: Upon successful authentication, the IDP generates a secure token or session cookie that represents the user's authenticated session.
5. Token exchange: The token is passed back to the application that initially requested authentication.
6. Application validation: The application verifies the token's authenticity and validity by communicating with the IDP or by checking the token's digital signature.
7. Access granted: If the token is valid, the application grants the user access without requiring them to provide additional credentials.

## How to use GID-SSO

On your login screen, redirect the user when logging in to `http://id.gabrikowski.de/sso/login`. You have to provide a redirect URL where the user will be redirected to after a successful login. Provide this via the query-parameter `redirect`.

Example:

`https://id.gabrikowski.de/sso/login?redirect=google.com`

The user will then be shown a login page where they input their credentials or use a cookie if they are already logged in. They can also register a new GID-Account if necessary.

After a successful login, the user will be redirected to the given redirect URL. To the redirect URL, we will add the query-parameters `token` and `uuid`. The `uuid` field is self-explanatory, and the token consists of the `userdata-payload` and a digital signature to validate the data (see below).

Example:

`https://google.com?token=[generated token]&uuid=70b4acde-0474-11ee-be56-0242ac120002`

(The UUID in this example is randomly generated and doesn't mean anything :D)

You can then store the UUID in your database to link your data to the user.

## How to validate the token

As mentioned above, the provided token consists of two parts: the userdata and a signature. The parts are both encoded as a HEX-String and are separated by an underscore.

The token is encrypted using a `private RSA key` and can be decrypted using a `public RSA key`. The public key can be downloaded here:

`https://id.gabrikowski.de/rsa-id`

It is also used to validate the signature.

You can then use any crypto library that supports `asymmetric RSA encryption` to get the userdata and validate the token.

Here's an example for Node.js using the built-in `crypto` module:

```javascript
const { publicDecrypt, createVerify } = require("crypto");

const PUBLIC_KEY = "--public RSA key of GID--";

let input_token = "--token provided by the SSO provider--";

// Split the token into the userdata and signature (separated by '_')
let token = input_token.split("_")[0]; // Get the encrypted userdata from the token
let signature = input_token.split("_")[1]; // Get the signature

// Returns the content of the token as a string (encoded in UTF-8).
let token_content = publicDecrypt(
	PUBLIC_KEY,
	Buffer.from(token, "hex")
).toString("utf-8");

// Parsing the userdata from the token
let userdata = JSON.parse(token_content);
console.log(userdata); // See result below \/

// Verify the signature
let verifier = createVerify("rsa-sha256");
verifier.update(token_content);
let isVerified = verifier.verify(PUBLIC_KEY, signature, "hex");

console.log(isVerified); // -> true or false
```

Userdata in the token:

```json
{
	"id": 1,
	"name": "John Doe",
	"username": "johndoe",
	"uuid": "663da258-0477-11ee-be56-0242ac120002"
}
```
