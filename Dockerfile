FROM node:16

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

ENV SERVER_PORT=3008

EXPOSE 3008

CMD ["npm", "start"]