const {
	generateKeyPairSync,
	createSign,
	createVerify,
	publicDecrypt,
	privateEncrypt,
} = require("crypto");

/**
 * Key Gen
 */
const { privateKey, publicKey } = generateKeyPairSync("rsa", {
	modulusLength: 2048,
	publicKeyEncoding: {
		type: "spki",
		format: "pem",
	},
	privateKeyEncoding: {
		type: "pkcs8",
		format: "pem",
	},
});

console.log(privateKey);
console.log(publicKey);

/**
 * Encrypt and Sign
 */

//define message
let message = "Verschlüsselung ist cool!";

// encrypt message (hex string)
let encryptedMessage = privateEncrypt(
	privateKey,
	Buffer.from(message)
).toString("hex");
console.log("Encrypted:", encryptedMessage, "\n");

// sign message
const signer = createSign("rsa-sha256");
signer.update(message);
let signature = signer.sign(privateKey, "hex");
console.log("Signature:", signature, "\n");

/**
 * Decrypt / verify
 */

// decrypt message
let decryptedMessage = publicDecrypt(
	publicKey,
	Buffer.from(encryptedMessage, "hex")
).toString("utf-8");
console.log("Decrypted:", decryptedMessage, "\n");

// verify
let verifier = createVerify("rsa-sha256");
verifier.update(decryptedMessage);
let isVerified = verifier.verify(publicKey, signature, "hex");
console.log("Verified:", isVerified, "\n");
