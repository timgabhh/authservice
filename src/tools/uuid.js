const uuid = require("uuid");

const NAMESPACE = "d4f03451-7626-4b21-bcc7-c4de689891b5";

function generateUUID(username) {
	return uuid.v5(username, NAMESPACE);
}
module.exports = {
	generateUUID,
};
