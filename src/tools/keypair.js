const { generateKeyPairSync } = require("crypto");
const fs = require("fs");
const path = require("path");

const KEY_PATH = path.join(__dirname, "..", "..", "keys");
const PUBLIC_KEY_PATH = path.join(KEY_PATH, "public.key");
const PRIVATE_KEY_PATH = path.join(KEY_PATH, "private.key");

if (!fs.existsSync(KEY_PATH)) {
	fs.mkdirSync(KEY_PATH);
}

if (!(fs.existsSync(PUBLIC_KEY_PATH) && fs.existsSync(PRIVATE_KEY_PATH))) {
	console.log("not existing");
	let newKeys = generateKeyPairSync("rsa", {
		modulusLength: 2048,
		publicKeyEncoding: {
			type: "spki",
			format: "pem",
		},
		privateKeyEncoding: {
			type: "pkcs8",
			format: "pem",
		},
	});

	fs.writeFileSync(PRIVATE_KEY_PATH, newKeys.privateKey);
	fs.writeFileSync(PUBLIC_KEY_PATH, newKeys.publicKey);
}

const privateKey = fs.readFileSync(PRIVATE_KEY_PATH);
const publicKey = fs.readFileSync(PUBLIC_KEY_PATH);

module.exports = {
	privateKey,
	publicKey,
};
