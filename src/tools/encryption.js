const { createSign, privateEncrypt } = require("crypto");
const { privateKey } = require("./keypair");

function encrypt(message) {
	return privateEncrypt(privateKey, Buffer.from(message)).toString("hex");
}

function sign(message) {
	const signer = createSign("rsa-sha256");
	signer.update(message);
	return signer.sign(privateKey, "hex");
}

module.exports = {
	encrypt,
	sign,
};
