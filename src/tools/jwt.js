const jwt = require("jsonwebtoken");

function generateAccessToken(user) {
	return jwt.sign(user, global.ACCESS_TOKEN_SECRET);
}

function validateToken(token, accept, reject) {
	jwt.verify(token, global.ACCESS_TOKEN_SECRET, (err, payload) => {
		if (err) return reject();
		accept(payload);
	});
}

module.exports = {
	generateAccessToken,
	validateToken,
};
