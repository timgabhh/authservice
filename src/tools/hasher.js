const crypto = require("crypto");

const ITERATIONS = 10000;

function hash(password) {
    var salt = crypto.randomBytes(16).toString('hex');
    var hash = crypto.pbkdf2Sync(password, salt, ITERATIONS, 64, "sha512").toString("hex");
    return hash + ";" + salt;
}

function verify(password, dbHash) {
    var split = dbHash.split(";");
    var hash = split[0];
    var salt = split[1];
    return hash == crypto.pbkdf2Sync(password, salt, ITERATIONS, 64, "sha512").toString("hex");
}

module.exports = { hash, verify };