const { DataTypes, Sequelize } = require("sequelize");
const con = require("./connection");

const User = con.define("user", {
	username: {
		type: DataTypes.STRING,
		unique: "username",
		allowNull: false,
	},
	passwordHash: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	name: {
		type: DataTypes.STRING,
	},
	email: {
		type: DataTypes.STRING,
	},
	uuid: {
		type: DataTypes.STRING,
		allowNull: false,
	},
});

con.sync({ alter: true });

module.exports = {
	User,
};
