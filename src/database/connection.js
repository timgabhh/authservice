const { Sequelize } = require('sequelize');
require("dotenv").config();

const connection = new Sequelize(process.env.DATABASE_NAME, process.env.DATABASE_USERNAME, process.env.DATABASE_PASSWORD, {
    port: process.env.DATABASE_PORT,
    host: process.env.DATABASE_HOST,
    logging: console.log,
    dialect: "mysql",
});

module.exports = connection;