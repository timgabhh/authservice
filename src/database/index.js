const { User } = require("./models");

function getAllUsers() {
	return User.findAll();
}
function registerUser(user) {
	return User.create(user);
}
function getUser(where) {
	return User.findOne({ where: where });
}

module.exports = {
	getAllUsers,
	registerUser,
	getUser,
	User,
};
