const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

let redirectUrl = new URL(window.location.origin);
try {
	redirectUrl = new URL(urlParams.get("redirect"));
	console.log(redirectUrl);
} catch {}

let cookieToken = cookies.get("loginToken");
let cookieUUID = cookies.get("uuid");

if (cookieToken !== null && cookieUUID !== null) {
	console.log("already logged in");
	redirectUrl.searchParams.append("token", cookieToken);
	redirectUrl.searchParams.append("uuid", cookieUUID);
	window.location.replace(redirectUrl);
}
let nameField = field_create({
	type: "text",
	placeholder: "Name",
	color: "#00ff00",
	validators: [
		(value) => {
			return value !== null && value !== undefined && value.length > 0;
		},
	],
});
let mailField = field_create({
	type: "email",
	placeholder: "E-Mail",
	color: "#00ff00",
	validators: [
		(value) => {
			return value.match(
				/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
			);
		},
		(value) => {
			return value !== null && value !== undefined && value.length > 0;
		},
	],
});
let usernameField = field_create({
	type: "text",
	placeholder: "Username",
	color: "#00ff00",
	validators: [
		(value) => {
			return value !== null && value !== undefined && value.length > 0;
		},
	],
});
let passwordField = field_create({
	type: "password",
	placeholder: "Password",
	color: "#00ff00",
	validators: [
		(value) => {
			return value !== null && value !== undefined && value.length > 0;
		},
	],
});
let usernameTakenBox = layout_create({
	color: "#ff0000",
	style: {
		width: "476px",
	},
	border: true,
	children: [
		text_create({
			text: "Username taken",
		}),
	],
});
usernameTakenBox.hide();

let errorBox = layout_create({
	color: "#ff0000",
	style: {
		width: "476px",
	},
	border: true,
	children: [
		text_create({
			text: "Something failed but I don't know what",
		}),
	],
});
errorBox.hide();

let defaultPage = page_create({
	id: "mainPage",
	title: "Register new GID",
	center: true,
	layout: "column",
	children: [
		text_create({
			text:
				"After that you will be logged in and redirected to " +
				redirectUrl.host,
		}),
		usernameTakenBox,
		nameField,
		usernameField,
		mailField,
		passwordField,
		button_create({
			label: "Register",
			color: "#00ff00",
			style: {
				width: "480px",
			},
			onclick: () => {
				registerSubmit();
			},
			plain: true,
		}),
	],
});
defaultPage.show();

async function registerSubmit() {
	usernameTakenBox.hide();

	let usernameValid = usernameField.valid();
	let passwordValid = passwordField.valid();
	let nameValid = nameField.valid();
	let mailValid = mailField.valid();

	if (!(usernameValid && passwordValid && nameValid && mailValid)) return;

	let success = await apiSubmitRegister();
	if (!success) return errorBox.show();

	await apiSubmitLogin();
}

async function apiSubmitRegister() {
	let payload = {
		username: usernameField.get(),
		password: passwordField.get(),
		name: nameField.get(),
		email: mailField.get(),
	};
	let response = await fetch(window.location.href, {
		method: "POST",
		headers: {
			Accept: "application/json",
			"Content-Type": "application/json",
		},
		body: JSON.stringify(payload),
	});
	if (response.status == 409) {
		usernameTakenBox.show();
		return false;
	} else if (response.status == 200) {
		return true;
	} else {
		return false;
	}
}

async function apiSubmitLogin() {
	let payload = {
		username: usernameField.get(),
		password: passwordField.get(),
	};
	let response = await fetch(new URL("sso/login", window.location.origin), {
		method: "POST",
		headers: {
			Accept: "application/json",
			"Content-Type": "application/json",
		},
		body: JSON.stringify(payload),
	});
	if (response.status == 401) {
		usernameTakenBox.show();
	} else if (response.status == 200) {
		let resData = await response.json();
		console.log(resData);

		let token = resData.token + "_" + resData.signature;
		console.log(token);
		cookies.set("loginToken", token);
		cookies.set("uuid", resData.uuid);

		redirectUrl.searchParams.append("token", token);
		redirectUrl.searchParams.append("uuid", resData.uuid);
		window.location.replace(redirectUrl);
	} else {
	}
	return {};
}
