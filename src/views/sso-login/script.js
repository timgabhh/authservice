const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

let redirectUrl = new URL(window.location.origin);
try {
	redirectUrl = new URL(urlParams.get("redirect"));
	console.log(redirectUrl);
} catch {}

let cookieToken = cookies.get("loginToken");
let cookieUUID = cookies.get("uuid");

if (cookieToken !== null && cookieUUID !== null) {
	console.log("already logged in");
	redirectUrl.searchParams.append("token", cookieToken);
	redirectUrl.searchParams.append("uuid", cookieUUID);
	window.location.replace(redirectUrl);
}

let usernameField = field_create({
	type: "text",
	placeholder: "Username",
	color: "#00ff00",
	validators: [
		(value) => {
			return value !== null && value !== undefined && value.length > 0;
		},
	],
});
let passwordField = field_create({
	type: "password",
	placeholder: "Password",
	color: "#00ff00",
	validators: [
		(value) => {
			return value !== null && value !== undefined && value.length > 0;
		},
	],
});
let invalidCredentialsBox = layout_create({
	color: "#ff0000",
	style: {
		width: "476px",
	},
	border: true,
	children: [
		text_create({
			text: "Invalid credentials",
		}),
	],
});
invalidCredentialsBox.hide();

let defaultPage = page_create({
	id: "mainPage",
	title: "Login to your GID",
	center: true,
	layout: "column",
	children: [
		text_create({
			text: "After that you will be redirected to " + redirectUrl.host,
		}),
		invalidCredentialsBox,
		usernameField,
		passwordField,
		button_create({
			label: "Login",
			color: "#00ff00",
			style: {
				width: "480px",
			},
			onclick: () => {
				registerSubmit();
			},
			plain: true,
		}),
		button_create({
			label: "I don't have an account",
			color: "#ff0000",
			style: {
				width: "480px",
			},
			onclick: () => {
				let registerUrl = new URL("sso/register", window.location.origin);
				registerUrl.searchParams.append("redirect", redirectUrl);
				console.log(registerUrl);
				window.location.replace(registerUrl);
			},
			plain: true,
		}),
	],
});
defaultPage.show();

function registerSubmit() {
	invalidCredentialsBox.hide();

	let usernameValid = usernameField.valid();
	let passwordValid = passwordField.valid();

	if (!(usernameValid && passwordValid)) return;

	apiSubmitLogin();
}

async function apiSubmitLogin() {
	let payload = {
		username: usernameField.get(),
		password: passwordField.get(),
	};
	let response = await fetch(window.location.href, {
		method: "POST",
		headers: {
			Accept: "application/json",
			"Content-Type": "application/json",
		},
		body: JSON.stringify(payload),
	});
	if (response.status == 401) {
		invalidCredentialsBox.show();
	} else if (response.status == 200) {
		let resData = await response.json();
		console.log(resData);

		let token = resData.token + "_" + resData.signature;
		console.log(token);
		cookies.set("loginToken", token);
		cookies.set("uuid", resData.uuid);

		redirectUrl.searchParams.append("token", token);
		redirectUrl.searchParams.append("uuid", resData.uuid);
		window.location.replace(redirectUrl);
	} else {
	}
}
