const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

let cookieToken = cookies.get("loginToken");
let cookieUUID = cookies.get("uuid");

if (cookieToken == null || cookieUUID == null) {
	window.location.replace(window.location.origin + "/sso/login");
}

let userDataText = text_create({
	text: "",
	style: { textAlign: "left", paddingBottom: "2.5px" },
});
let defaultPage = page_create({
	id: "mainPage",
	title: "Welcome to GID",
	center: true,
	layout: "column",
	children: [
		text_create({
			text: "You are logged in and ready to use any Service that uses GID-SSO",
		}),
		userDataText,
		button_create({
			label: "Logout",
			color: "#ff0000",
			style: {
				width: "150px",
			},
			onclick: () => {
				cookies.erase("loginToken");
				cookies.erase("uuid");
				window.location.reload();
			},
		}),
	],
});
defaultPage.show();

getUser(cookieUUID);

async function getUser(uuid) {
	let response = await fetch(window.location.origin + "/users/" + uuid);
	let user = await response.json();
	let str = "Userdata: \n";
	str += "UUID:       " + user.uuid + "\n";
	str += "Name:       " + user.name + "\n";
	str += "Username:   " + user.username + "\n";
	str += "E-Mail:     " + (user.email || "---");
	userDataText.change(str);
}
