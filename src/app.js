const express = require("express");
const cors = require("cors");
const { randomBytes } = require("crypto");
const usersRouter = require("./routes/users");
const authRouter = require("./routes/auth");
const ssoRouter = require("./routes/sso");

global.ACCESS_TOKEN_SECRET = randomBytes(32).toString("hex");

const app = express();

app.use(cors());
app.use(express.json());

app.use("/users", usersRouter);
app.use("/", authRouter);
app.use("/sso", ssoRouter);

app.listen(3008, () => {
	console.log("Working on port 3008");
});

process.on("uncaughtException", (error, origin) => {
	console.log(error);
	console.log(origin);
});
