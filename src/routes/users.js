const router = require("express").Router();

const db = require("../database");

router.get("/", (req, res) => {
	res.send({ message: "ok" });
});
router.get("/:uuid", async (req, res) => {
	const { uuid } = req.params;
	console.log(uuid);
	let user = await db.User.findOne({
		where: { uuid: uuid },
		attributes: ["uuid", "username", "name", "email"],
	});
	if (!user) return res.sendStatus(404);
	res.send(user);
});

module.exports = router;
