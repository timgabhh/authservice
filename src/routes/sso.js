const router = require("express").Router();
const express = require("express");
const { verify, hash } = require("../tools/hasher");
const { User } = require("../database");
const { encrypt, sign } = require("../tools/encryption");
const { generateUUID } = require("../tools/uuid");
const { body, validationResult } = require("express-validator");

const path = require("path");
router.use(
	"/login",
	express.static(path.join(__dirname, "..", "views", "sso-login"))
);
router.use(
	"/register",
	express.static(path.join(__dirname, "..", "views", "sso-register"))
);
router.post("/login", async (req, res) => {
	const { username, password } = req.body;

	let user = await User.findOne({ where: { username: username } });
	if (user === null || user === undefined) return res.sendStatus(401);

	if (!verify(password, user.dataValues.passwordHash))
		return res.sendStatus(401);

	// generate Token
	let resUser = {
		id: user.dataValues.id,
		name: user.dataValues.name,
		username: user.dataValues.username,
		uuid: user.dataValues.uuid,
	};
	let userString = JSON.stringify(resUser);

	let token = encrypt(userString);
	let signature = sign(userString);

	res.send({
		token: token,
		signature: signature,
		uuid: resUser.uuid,
	});
});
router.post(
	"/register",
	body("username").isString(),
	body("password").isString().isLength({ min: 4 }),
	body("name").isString(),
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty())
			return res.status(400).json({ errors: errors.array() });

		const { username, name, password, email } = req.body;

		var user = {
			username: username,
			passwordHash: hash(password),
			name: name,
			uuid: generateUUID(username),
			email: email,
		};

		console.log(user);
		let regUser = await User.create(user).catch((error) => {
			if (error.name == "SequelizeUniqueConstraintError") {
				return res.status(409).send({ message: "Username taken" });
			}
			console.log(error);
		});
		regUser.passwordHash = undefined;
		res.send(regUser);
	}
);

module.exports = router;
