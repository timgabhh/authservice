const router = require("express").Router();
const express = require("express");
const { hash, verify } = require("../tools/hasher");
const { generateAccessToken, validateToken } = require("../tools/jwt");
const { body, validationResult } = require("express-validator");
const { User } = require("../database");
const db = require("../database");
const { generateUUID } = require("../tools/uuid");
const path = require("path");
const { publicKey } = require("../tools/keypair");

router.use("/", express.static(path.join(__dirname, "..", "views", "home")));

router.get("/rsa-id", (req, res) => {
	res.send(publicKey);
});

router.post("/validate", (req, res) => {
	const token = req.body.token;
	validateToken(
		token,
		(payload) => {
			res.send({ valid: true, user: payload.user });
		},
		() => {
			res.status(401).send({ valid: false });
		}
	);
});

router.post(
	"/register",
	body("username").isString(),
	body("password").isString().isLength({ min: 4 }),
	body("name").isString(),
	(req, res) => {
		console.log("register");
		const errors = validationResult(req);
		if (!errors.isEmpty())
			return res.status(400).json({ errors: errors.array() });

		const { username, name, password } = req.body;

		var user = {
			username: username,
			passwordHash: hash(password),
			name: name,
			uuid: generateUUID(username),
		};

		console.log(user);
		db.registerUser(user)
			.then((nUser) => {
				nUser.passwordHash = undefined;
				res.send(nUser);
			})
			.catch((error) => {
				if (error.name == "SequelizeUniqueConstraintError") {
					return res.status(409).send({ message: "Username taken" });
				}
				console.log(error);
			});
	}
);

router.post(
	"/login",
	body("username").isString(),
	body("password").isString(),
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty())
			return res.status(400).json({ errors: errors.array() });

		const { username, password } = req.body;

		let user = await User.findOne({ where: { username: username } });
		if (user === null || user === undefined) return res.sendStatus(400);

		if (!verify(password, user.dataValues.passwordHash))
			return res.sendStatus(401);

		user.passwordHash = undefined;
		var token = generateAccessToken({ verify: "/verify", user });

		res.send({ token: token, verifyRoute: "/verify" });
	}
);

module.exports = router;
